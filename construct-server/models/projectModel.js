'use strict';

var mongoose = require('mongoose'),
    GeoJSON = require('mongoose-geojson-schema'),
    Schema = mongoose.Schema;

var ScanDetails = new Schema({

    name: { // scan type Ex:'Exterior', 'Interior', 'Survey'
        type: String,
        trim: true,
        enum: ['Exterior', 'Interior', 'Survey'],
        required: true
    },

    area: {

        quantity: {
            type: Number,
            required: true
        },
        units: {
            type: String,
            required: true
        }
    },

    repeat: {

        count: { //     Number of times to capture data
            type: Number,
            require: true
        },
        frequency: { //     Scan frequency
            type: String,
            trim: true,
            enum: ['Day', 'Week', 'Month'],
            required: true
        }
    }
});

var ProjectSchema = new Schema({

    name: { //  name of the project

        type: String,
        trim: true,
        required: true
    },

    location: { // property loaction latlang where it is existed
        type: [Number],
        required: true
    },

    address: { // property address
        type: String,
        trim: true,
        required: true
    },

    description: { //  few lines description about the project

        type: String,
        trim: true
    },

    organisation: { //  object Id of the organization under which this project is created

        type: Schema.Types.ObjectId,
        ref: 'Organisation',
        required: true
    },

    scanDetails: {

        exterior: ScanDetails,

        interior: ScanDetails,

        survey: ScanDetails
    },

    projectType: { //  Type of project. Currently suppourted values construction-management, MAUD, smart-city

        type: String,
        trim: true,
        default: 'Other'
    },

    createdBy: { //  object Id of user who created this project

        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    parent: { //    parent project for this project

        type: Schema.Types.ObjectId,
        ref: 'Project'
    },

    plannedStartDate: Date, // Equals to the Min Start Date of the sub-projects (Planned)

    plannedFinishDate: Date, // Equals to the Max End Date of the sub-projects (Planned)

    plannedCost: Number, // planned cost of the project in INR.

    actualStartDate: Date, // Equals to the Min Start Date of the sub-projects (Actual). Forecasted for future dates

    actualFinishDate: Date, // Equals to the Max End Date of the sub-projects (Actual). Forecasted for future dates

    actualCost: Number, // actual cost (till date) of the project in INR.

    snapshot: [{ //  quantity (progress) date-wise

        date: Date,
        actual: Number,
        planned: Number
    }],

    projects: [{ //     all the subprojects for this project

        type: Schema.Types.ObjectId,
        ref: 'Project'
    }],

    thumbnail: String, // thumbnail of the project

    geometry: mongoose.Schema.Types.Geometry,

    tilesets: [{

        type: Schema.Types.ObjectId,
        ref: 'Tileset'

    }] // Object ids of all the tilesets associated to this project

}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

ProjectSchema.virtual('progress').get(function () {
    try {
        if (this.snapshot && this.snapshot.length > 0) {
            let today = new Date();
            let actual_index = -1;
            let planned_index = -1;
            for (let i = this.snapshot.length - 1; i > -1; i--) {
                if (this.snapshot[i].actual !== undefined && this.snapshot[i].date <= today) {
                    actual_index = i;
                    break;
                }
            }
            for (let i = this.snapshot.length - 1; i > -1; i--) {
                if (this.snapshot[i].planned !== undefined && this.snapshot[i].date <= today) {
                    planned_index = i;
                    break;
                }
            }
            if (this.snapshot.length > actual_index && actual_index > -1 && this.snapshot.length > planned_index && planned_index > -1) {
                let mProgress = {
                    date: '',
                    actual: 0,
                    planned: 0,
                    actual_progress: 0,
                    planned_progress: 0
                }
                let actual_progress = this.snapshot[actual_index];
                let planned_progress = this.snapshot[planned_index];
                mProgress.date = actual_progress.date;
                mProgress.actual = actual_progress.actual;
                mProgress.planned = planned_progress.planned;
                if (actual_progress.actual) {
                    mProgress.actual_progress = actual_progress.actual;
                }
                mProgress.planned_progress = planned_progress.planned;
                return mProgress;
            } else {
                return {
                    planned: 0,
                    actual: 0
                };
            }
        } else {
            return {
                date: new Date(),
                planned: 0,
                actual: 0
            };
        }
    } catch (err) {
        console.log(err);
        return {
            planned: 0,
            actual: 0
        };
    }
});


module.exports = mongoose.model('Project', ProjectSchema);