'use strict';

var blogic = require('../lib/blogic'),
    logger = require('../utils/logger').getLogger('Annotation Controller');

module.exports = {

    listAnnotations: function (req, res) {
        listAnnotations(req, res)
    },

    getAnnotationDetails: function (req, res) {
        getAnnotationDetails(req, res)
    },

    createAnnotation: function (req, res) {
        createAnnotation(req, res)
    },

    updateAnnotation: function (req, res) {
        updateAnnotation(req, res)
    },

    deleteAnnotation: function (req, res) {
        deleteAnnotation(req, res)
    }
}

/**
 *   retrieve the annotations associated to the orthomosaic
 */

var listAnnotations = async function (req, res) {
    try {
        let resObj = await blogic.annotations.listAnnotations(req.query);
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res);
    }
}

/**
 *   Get the annotation info from object id
 */
var getAnnotationDetails = async function (req, res) {
    try {
        let resObj = await blogic.annotations.getAnnotation({
            _id: req.params.annoId
        });
        resObj.status = 'success';
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res);
    }
}

/**
 *   create a new annotation and assign to an orthomosaic
 */
var createAnnotation = async function (req, res) {
    try {
        let resObj = await blogic.annotations.createAnnotation(req.body, req.decoded.user);
        resObj.status = 'success';
        logger.info("Request fullfilled - 201 CREATED");
        return res.status(201).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res);
    }
};

/**
 *   update annotation
 */
var updateAnnotation = async function (req, res) {
    try {
        let resObj = await blogic.annotations.updateAnnotation(req.params.annoId, req.body);
        resObj.status = 'success';
        logger.info("Request fullfilled - 201 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res);
    }
};

/**
 *   delete annotation
 */
var deleteAnnotation = async function (req, res) {
    try {
        await blogic.annotations.removeAnnotation(req.params.annoId);
        let resObj = {};
        resObj.status = 'success';
        logger.info("Request fullfilled - 204 DELETED");
        return res.status(204).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res);
    }
};


/**
 *   HANDLE ERROR MESSAGES
 */
async function processErrorMessage(err, res) {
    let resObj = {
        success: false,
        message: err.message
    };
    return res.status(err.code ? err.code : 500).json(resObj);
}
