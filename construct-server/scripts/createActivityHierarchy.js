'use strict'

const mongodb = require('../config/mongodb');
const blogic = require('../lib/blogic');

let data = require('./files/file.json');

createActivities().then(res => console.log(res)).catch(err => console.log(err));

async function createActivities() {
    for(let i = 0; i < data.length; i++) {
        let activity = await createActivity(data[i]);
        console.log(activity._id, activity.wbsId, activity.name);
    }
}

async function createActivity(body) {
    return await blogic.activities.createActivity(body);
}

module.exports = {
}