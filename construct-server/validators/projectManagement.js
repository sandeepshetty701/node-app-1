var { check } = require('express-validator/check'),
    utils = require('../utils/projects'),
    mongodb = require('../lib/storage/mongodb');

module.exports = {

    checkCreateProject: [
        check('name').exists().withMessage('Please Provide Project Name'),
        check('organisation').exists().withMessage('Please Provide Organisation'),
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                let permission = '';
                if(req.body.parent) {
                    permission = 'update-project';
                } else {
                    permission = 'create-project';
                }
                await utils.checkForPermission(value, req.body.parent ? null : req.body.organisation, req.body.parent, permission);
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ],

    checkUpdateProject: [
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                await utils.checkForPermission(value, null, req.params.id, 'update-project');
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ],

    checkDeleteProject: [
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                await utils.checkForPermission(value, null, req.params.id, 'delete-project');
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ],

    checkMakeProjectAdmin: [
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                await utils.checkForPermission(value, null, req.params.id, 'make-project-admin');
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ],

    checkRemoveProjectAdmin: [
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                await utils.checkForPermission(value, null, req.params.id, 'remove-project-admin');
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ],

    checkAddProjectUser: [
        check('x-auth-token').custom(async (value, { req }) => {
            try {
                await utils.checkForPermission(value, null, req.params.id, 'add-project-user');
            } catch (err) {
                throw(err);
            }
            return true;
        })
    ]
}

