var lodash = require('lodash');

const mongoose = require('mongoose');

var mongodb = require('../../config/mongodb');

//models
var Organisation = mongodb.models.Organisation;
var Activity = mongodb.models.Activity;
var UserToken = mongodb.models.UserToken;
var Project = mongodb.models.Project;
var Annotation = mongodb.models.Annotation;
var Tileset = mongodb.models.Tileset;


let organisations = {

    getOrganisations: async function (query, populate) {
        try {
            let organisationQuery = Organisation.find(query);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    organisationQuery.populate(populate[i]);
                }
            }
            return await organisationQuery.exec();
        } catch (err) {
            throw (err)
        }
    },

    getOrganisation: async function (query, select, populate) {
        try {
            let organisationQuery = Organisation.findOne(query, select);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    organisationQuery.populate(populate[i]);
                }
            }
            return await organisationQuery.exec();
        } catch (err) {
            throw (err)
        }
    },

    createOrganisation: async function (body) {
        try {
            let newOrganisation = new Organisation(body);
            return await newOrganisation.save();
        } catch (err) {
            throw (err)
        }
    },

    updateOrganisation: async function (query, updateObj) {
        try {
            return await Organisation.findAndUpdate(query, updateObj, {
                new: true
            }).exec();
        } catch (err) {
            throw (err)
        }
    },

    deleteOrganisation: async function (query) {
        try {
            return await Organisation.deleteOne(query).exec();
        } catch (err) {
            throw (err)
        }
    }

}

var projects = {

    getProjects: async function (query, selection, populate) {
        try {
            let projectQuery = Project.find(query, selection);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    projectQuery.populate(populate[i]);
                }
            }
            return await projectQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    getProject: async function (query, selection, populate) {
        try {
            let projectQuery = Project.findOne(query, selection);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    projectQuery.populate(populate[i]);
                }
            }
            return await projectQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    updateProject: async function (query, data) {
        try {
            return await Project.findOneAndUpdate(query, data, {
                new: true
            }).exec();
        } catch (err) {
            throw (err);
        }
    },

    removeProject: async function (query) {
        try {
            return await Project.remove(query).exec();
        } catch (err) {
            throw (err);
        }
    },

    getProjectHierarchy: async function (project) {
        try {
            return await Project.findOne({
                _id: project
            }, "_id name projects")
                .populate({
                    path: 'projects',
                    model: 'Project',
                    select: '_id name projects',
                    populate: {
                        path: 'projects',
                        model: 'Project',
                        select: '_id name projects',
                        populate: {
                            path: 'projects',
                            model: 'Project',
                            select: '_id name projects',
                            populate: {
                                path: 'projects',
                                model: 'Project',
                                select: '_id name projects',
                                populate: {
                                    path: 'projects',
                                    model: 'Project',
                                    select: '_id name projects',
                                    populate: {
                                        path: 'projects',
                                        model: 'Project',
                                        select: '_id name projects'
                                    }
                                }
                            }
                        }
                    }
                });
        } catch (err) {
            throw (err);
        }
    },

    getProjectProgressForDate: async function (date, query) {
        try {
            let progress = await Project.aggregate([{
                $match: {
                    parent: query ? new mongoose.Types.ObjectId(query) : null
                }
            },
            {
                $unwind: '$snapshot'
            },
            {
                $sort: {
                    'snapshot.date': 1
                }
            },
            {
                $group: {
                    _id: '$name',
                    weightage: {
                        $avg: {
                            $cond: {
                                if: {
                                    $eq: ['$properties.ignore', true]
                                },
                                then: 0,
                                else: '$weightage'
                            }
                        }
                    },
                    scope_quantity: {
                        $avg: '$scope.quantity'
                    },
                    snapshot: {
                        $push: '$snapshot'
                    }
                }
            },
            {
                $project: {
                    snapshot_actual: {
                        $arrayElemAt: [{
                            $slice: [{
                                $filter: {
                                    input: '$snapshot',
                                    as: 'snapshot',
                                    cond: {
                                        $and: [
                                            { $lte: ['$$snapshot.date', date] },
                                            { $ne: [{ $type: '$$snapshot.actual' }, 'missing'] }
                                        ]
                                    }
                                }
                            }, -1]
                        },
                            0
                        ]
                    },
                    snapshot_planned: {
                        $arrayElemAt: [{
                            $slice: [{
                                $filter: {
                                    input: '$snapshot',
                                    as: 'snapshot',
                                    cond: {
                                        $and: [
                                            { $lte: ['$$snapshot.date', date] }
                                        ]
                                    }
                                }
                            }, -1]
                        },
                            0
                        ]
                    },
                    weightage: 1,
                    scope_quantity: 1,
                    wbs: 1,
                    _id: 1
                }
            },
            {
                $project: {
                    name: '$_id',
                    actual: {
                        $cond: {
                            if: {
                                $eq: [{
                                    $type: '$snapshot_actual.actual'
                                }, 'missing']
                            },
                            then: {
                                $sum: -1
                            },
                            else: {
                                $cond: {
                                    if: {
                                        $eq: ['$scope_quantity', 0]
                                    },
                                    then: 100,
                                    else: {
                                        $divide: [{
                                            $multiply: [{
                                                $sum: '$snapshot_actual.actual'
                                            }, 100]
                                        }, '$scope_quantity']
                                    }
                                }
                            }
                        }
                    },
                    weightage: '$weightage',
                    planned: {
                        $cond: {
                            if: {
                                $eq: ['$scope_quantity', 0]
                            },
                            then: 100,
                            else: {
                                $divide: [{
                                    $multiply: [{
                                        $sum: '$snapshot_planned.planned'
                                    }, 100]
                                }, '$scope_quantity']
                            }
                        }
                    },
                    _id: 0,
                    otherField: 1
                }
            },
            {
                $group: {
                    _id: 'weighted average',
                    num_actual: {
                        $sum: {
                            $multiply: ['$weightage', '$actual']
                        }
                    },
                    num_planned: {
                        $sum: {
                            $multiply: ['$weightage', '$planned']
                        }
                    },
                    denominator: {
                        $sum: '$weightage'
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    date: date,
                    actual: {
                        $cond: {
                            if: {
                                $lt: ['$num_actual', 0]
                            },
                            then: '$REMOVE',
                            else: {
                                $min: [100, {
                                    $divide: ['$num_actual', '$denominator']
                                }]
                            }
                        }
                    },
                    planned: {
                        $min: [100, {
                            $divide: ['$num_planned', '$denominator']
                        }]
                    }
                }
            }
            ]).exec();
            if (!progress || progress.length === 0) {
                return {
                    date: date,
                    actual: 0,
                    planned: 0
                };
            } else {
                return progress[0];
            }
        } catch (err) {
            // console.log(err);
            return {
                date: date,
                actual: 0,
                planned: 0
            };
        }
    },

    getProjectAndChildProjectIds: async function (project) {
        try {
            let match_query = {};
            if (project) {
                match_query['_id'] = new mongoose.Types.ObjectId(project);
            } else {
                match_query['parent'] = null;
            }

            let projectids = await Project.aggregate([{
                    $match: match_query
                },
                {
                    $graphLookup: {
                        from: 'projects',
                        startWith: '$_id',
                        connectFromField: 'projects',
                        connectToField: '_id',
                        as: 'projectList'
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'projectList._id': 1
                    }
                },
                {
                    $unwind: '$projectList'
                },
                {
                    $group: {
                        '_id': null,
                        'projectIds': {
                            $addToSet: '$projectList._id'
                        }
                    }
                },
                {
                    $project: {
                        _id: 0
                    }
                }
            ]);
            return JSON.parse(JSON.stringify(projectids));
        } catch (err) {
            throw (err);
        }
    },

    updateProjects: async function (query, data) {
        try {
            return await Project.updateMany(query, data);
        } catch (err) {
            throw (err);
        }
    },

    getProjectAndParentProjectIds: async function (project) {
        try {
            let match_query = {};
            match_query['_id'] = new mongoose.Types.ObjectId(project);
            let projectids = await Project.aggregate([{
                    $match: match_query
                },
                {
                    $graphLookup: {
                        from: 'projects',
                        startWith: '$_id',
                        connectFromField: 'parent',
                        connectToField: '_id',
                        as: 'projectList'
                    }
                },
                {
                    $project: {
                        '_id': 0,
                        'projectList._id': 1
                    }
                },
                {
                    $unwind: '$projectList'
                },
                {
                    $group: {
                        '_id': null,
                        'projectIds': {
                            $addToSet: '$projectList._id'
                        }
                    }
                },
                {
                    $project: {
                        _id: 0
                    }
                }
            ]);
            return projectids;
        } catch (err) {
            throw (err);
        }
    },

}

var activities = {

    listActivities: async function (query, populate) {
        try {
            let activityQuery = Activity.find(query);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    activityQuery.populate(populate[i]);
                }
            }
            return await activityQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    getActivity: async function (query, populate) {
        try {
            let activityQuery = Activity.findOne(query);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    activityQuery.populate(populate[i]);
                }
            }
            return await activityQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    createActivity: async function (body, user) {
        try {
            var activity = new Activity(body);
            activity.createdBy = user;
            if (!activity.owner) {
                activity.owner = activity.createdBy;
            }
            return await activity.save();
        } catch (err) {
            throw (err);
        }
    },

    bulkCreateActivities: async function (body) {
        try {
            return await Activity.insertMany(body);
        } catch (err) {
            throw (err)
        }
    },

    updateActivity: async function (query, data, isNew) {
        try {
            if (!isNew)
                isNew = false;
            return await Activity.findOneAndUpdate(query, data, {
                new: isNew
            }).exec();
        } catch (err) {
            throw (err);
        }
    },

    getActivityProgressForDate: async function (date, query) {
        try {
            let progress = await Activity.aggregate([{
                $match: query
            },
            {
                $unwind: '$snapshot'
            },
            {
                $sort: {
                    'snapshot.date': 1
                }
            },
            {
                $group: {
                    _id: '$name',
                    weightage: {
                        $avg: {
                            $cond: {
                                if: {
                                    $eq: ['$properties.ignore', true]
                                },
                                then: 0,
                                else: '$weightage'
                            }
                        }
                    },
                    scope_quantity: {
                        $avg: '$scope.quantity'
                    },
                    snapshot: {
                        $push: '$snapshot'
                    }
                }
            },
            {
                $project: {
                    snapshot_actual: {
                        $arrayElemAt: [{
                            $slice: [{
                                $filter: {
                                    input: '$snapshot',
                                    as: 'snapshot',
                                    cond: {
                                        $and: [
                                            { $lte: ['$$snapshot.date', date] },
                                            { $ne: [{ $type: '$$snapshot.actual' }, 'missing'] }
                                        ]
                                    }
                                }
                            }, -1]
                        },
                            0
                        ]
                    },
                    snapshot_planned: {
                        $arrayElemAt: [{
                            $slice: [{
                                $filter: {
                                    input: '$snapshot',
                                    as: 'snapshot',
                                    cond: {
                                        $and: [
                                            { $lte: ['$$snapshot.date', date] }
                                        ]
                                    }
                                }
                            }, -1]
                        },
                            0
                        ]
                    },
                    weightage: 1,
                    scope_quantity: 1,
                    wbs: 1,
                    _id: 1
                }
            },
            {
                $project: {
                    name: '$_id',
                    actual: {
                        $cond: {
                            if: {
                                $eq: [{
                                    $type: '$snapshot_actual.actual'
                                }, 'missing']
                            },
                            then: {
                                $sum: -1
                            },
                            else: {
                                $cond: {
                                    if: {
                                        $eq: ['$scope_quantity', 0]
                                    },
                                    then: 100,
                                    else: {
                                        $divide: [{
                                            $multiply: [{
                                                $sum: '$snapshot_actual.actual'
                                            }, 100]
                                        }, '$scope_quantity']
                                    }
                                }
                            }
                        }
                    },
                    weightage: '$weightage',
                    planned: {
                        $cond: {
                            if: {
                                $eq: ['$scope_quantity', 0]
                            },
                            then: 100,
                            else: {
                                $divide: [{
                                    $multiply: [{
                                        $sum: '$snapshot_planned.planned'
                                    }, 100]
                                }, '$scope_quantity']
                            }
                        }
                    },
                    _id: 0,
                    otherField: 1
                }
            },
            {
                $group: {
                    _id: 'weighted average',
                    num_actual: {
                        $sum: {
                            $multiply: ['$weightage', '$actual']
                        }
                    },
                    num_planned: {
                        $sum: {
                            $multiply: ['$weightage', '$planned']
                        }
                    },
                    denominator: {
                        $sum: '$weightage'
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    date: date,
                    actual: {
                        $cond: {
                            if: {
                                $lt: ['$num_actual', 0]
                            },
                            then: '$REMOVE',
                            else: {
                                $min: [100, {
                                    $divide: ['$num_actual', '$denominator']
                                }]
                            }
                        }
                    },
                    planned: {
                        $min: [100, {
                            $divide: ['$num_planned', '$denominator']
                        }]
                    }
                }
            }
            ]).exec();
            if (!progress || progress.length === 0) {
                return {
                    date: date,
                    actual: 0,
                    planned: 0
                };
            } else {
                return progress[0];
            }
        } catch (err) {
            console.log(err);
            return {
                date: date,
                actual: 0,
                planned: 0
            };
        }
    },

    deleteActivity: async function (query) {
        try {
            return await Activity.deleteMany(query).exec();
        } catch (err) {
            throw (err);
        }
    }

}

var annotations = {

    getAnnotations: async function (query, populate) {
      try {
        let annotationQuery = Annotation.find(query, '-__v');
        if (populate) {
          for (let i = 0; i < populate.length; i++) {
            annotationQuery.populate(populate[i]);
          }
        }
        return await annotationQuery.exec();
      } catch (err) {
        throw (err)
      }
    },
  
    createAnnotation: async function (body) {
      try {
        var newAnnotation = new Annotation(body);
        return await newAnnotation.save();
      } catch (err) {
        throw (err);
      }
    },
  
    getAnnotation: async function (query, populate) {
      try {
        let annotationQuery = Annotation.findOne(query, '-__v');
        if (populate) {
          for (let i = 0; i < populate.length; i++) {
            annotationQuery.populate(populate[i]);
          }
        }
        return await annotationQuery.exec();
      } catch (err) {
        throw (err);
      }
    },
  
    updateAnnotation: async function (query, data) {
      try {
        return await Annotation.findOneAndUpdate(query, data, {
          new: true
        }).exec();
      } catch (err) {
        throw (err);
      }
    },
  
    removeAnnotation: async function (query) {
      try {
        return await Annotation.remove(query);
      } catch (err) {
        throw (err);
      }
    }
  }

async function getUserToken(token) {
    try {
      return await UserToken.findOne({
        token: token
      }).exec();
    } catch (err) {
      throw (err)
    }
  }

  var tilesets = {

    listTilesets: async function (query, selection, populate) {
        try {
            let tilesetQuery = Tileset.find(query, selection);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    tilesetQuery.populate(populate[i]);
                }
            }
            return await tilesetQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    createTileset: async function (body) {
        try {
            var newTileset = new Tileset(body);
            return await newTileset.save();
        } catch (err) {
            throw (err);
        }
    },

    getTileset: async function (query, populate) {
        try {
            let tilesetQuery = Tileset.findOne(query, selection);
            if (populate) {
                for (let i = 0; i < populate.length; i++) {
                    tilesetQuery.populate(populate[i]);
                }
            }
            return await tilesetQuery.exec();
        } catch (err) {
            throw (err);
        }
    },

    updateTileset: async function (query, data) {
        try {
            return await Tileset.findOneAndUpdate(query, data, {
                new: true
            }).exec();
        } catch (err) {
            throw (err);
        }
    },

    removeTileset: async function (query) {
        try {
            return await Tileset.delete(query).exec();
        } catch (err) {
            throw (err);
        }
    }
}


module.exports = {
    organisations: organisations,
    projects: projects,
    activities: activities,
    annotations: annotations,
    getUserToken: getUserToken,
    tilesets: tilesets
}