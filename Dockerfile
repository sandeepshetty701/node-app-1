FROM node:10-alpine

# WORKDIR specifies the application directory
WORKDIR /app

# Copying package.json file to the app directory
COPY construct-server/package.json /app

# Installing npm for DOCKER
RUN npm install

# Copying rest of the application to app directory
COPY . /app

EXPOSE 4001

# Starting the application using npm start
CMD ["npm","start"]
